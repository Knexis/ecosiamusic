package com.ecosia.music.utils;

import android.content.ContentUris;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.net.Uri;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class TrackUtils {

    public static Uri getPathURI(long id){
        return ContentUris.withAppendedId(android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, id);
    }

    public static Bitmap getCoverArt(Context context, long id){
        MediaMetadataRetriever mmr = new MediaMetadataRetriever();
        Uri uri = getPathURI(id);
        mmr.setDataSource(context, uri);

        byte [] data = mmr.getEmbeddedPicture();

        if(data != null)
            return BitmapFactory.decodeByteArray(data, 0, data.length);
        else
            return null;
    }

    public static CharSequence getTimeElasped(long duration) {
        SimpleDateFormat df = new SimpleDateFormat("mm:ss", Locale.US);
        return df.format(new Date(duration));
    }
}
