package com.ecosia.music.service;

import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.IBinder;
import android.view.View;
import android.widget.RemoteViews;

import androidx.annotation.Nullable;

import com.ecosia.music.listener.MusicPlayerListener;
import com.ecosia.music.model.MusicCursor;
import com.ecosia.music.model.MusicPlayer;
import com.ecosia.music.widget.MusicWidget;
import com.ecosia.music.R;
import com.ecosia.music.model.Track;
import com.ecosia.music.model.TrackNotification;
import com.ecosia.music.utils.TrackUtils;

import java.io.IOException;

public class MusicWidgetService extends Service implements MusicPlayerListener {

    private static final int MUSIC_SERVICE_NOTIFICATION_ID = 99;

    private MusicPlayer musicPlayer;
    private TrackNotification notification;
    private Handler mHandler = new Handler();

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        musicPlayer = new MusicPlayer(this);
        musicPlayer.setListener(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        String action = intent.getAction();
        boolean isPlaying = musicPlayer.isPlaying();

        if(action == null)
            return super.onStartCommand(intent, flags, startId);

        switch (action){
            case MusicWidget.ACTION_PLAY_N_PAUSE:

                if(isPlaying){
                    pauseTrack();
                }else
                    playTrack();

                displayTrackDetails(isPlaying);
                break;
            case MusicWidget.ACTION_STOP:
                onStopTrackActionReceived();
                break;
        }


        return super.onStartCommand(intent, flags, startId);
    }

    private void playTrack(){

        try {
            musicPlayer.play();
            mHandler.postDelayed(mUpdateTimeTask, 100);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void pauseTrack(){

        musicPlayer.pause();
        mHandler.removeCallbacks(mUpdateTimeTask);

    }

    //Stop track and show tracked stopped ui
    private void onStopTrackActionReceived(){
        stopTrack();
        displayStoppedView();
        stopSelf();
    }

    //Stop track and stop track elapsed time update
    private void stopTrack(){

        musicPlayer.stop();
        mHandler.removeCallbacks(mUpdateTimeTask);

    }

    //show tracked playing or paused ui based on isPlaying
    private void displayTrackDetails(Boolean isPlaying){

        Track track = MusicCursor.newInstance(this).getCurrentTrack();
        showNotification(track, isPlaying);

        RemoteViews views = MusicWidget.getRemoteViews(this);

        views.setTextViewText(R.id.tv_title, track.getTitle());
        views.setTextViewText(R.id.tv_description, track.getArtist());
        views.setTextViewText(R.id.tv_duration, TrackUtils.getTimeElasped(track.getDuration()));

        views.setViewVisibility(R.id.tv_description, View.VISIBLE);
        views.setViewVisibility(R.id.tv_duration, View.VISIBLE);
        views.setViewVisibility(R.id.img_stop, View.VISIBLE);

        views.setImageViewResource(R.id.img_play_n_pause, isPlaying?
                R.drawable.ic_play_arrow_black_24dp : R.drawable.ic_pause_black_24dp);

        Bitmap coverArt = TrackUtils.getCoverArt(this, track.getId());
        if(coverArt != null)
            views.setImageViewBitmap(R.id.img_cover_art, coverArt);
        else
            views.setImageViewResource(R.id.img_cover_art, R.drawable.ic_music_note_black_24dp);

        updateAppWidget(views);
    }

    //show tracked stopped ui
    private void displayStoppedView(){
        showNotification(null, false);

        RemoteViews views = MusicWidget.getRemoteViews(this);

        views.setTextViewText(R.id.tv_title, getString(R.string.tap_play_to_start));

        views.setImageViewResource(R.id.img_play_n_pause, R.drawable.ic_play_arrow_black_24dp);
        views.setViewVisibility(R.id.tv_description, View.GONE);
        views.setViewVisibility(R.id.img_stop, View.GONE);
        views.setViewVisibility(R.id.tv_duration, View.GONE);

        views.setImageViewResource(R.id.img_cover_art, R.drawable.ic_music_note_black_24dp);

        updateAppWidget(views);
    }

    //Show elapsed time
    private void displayTrackElapsedTime(CharSequence duration, CharSequence timeElapsed){
        RemoteViews views = MusicWidget.getRemoteViews(this);

        views.setTextViewText(R.id.tv_duration, timeElapsed +" - "+duration);

        views.setViewVisibility(R.id.tv_duration, View.VISIBLE);
        views.setViewVisibility(R.id.img_stop, View.VISIBLE);

        updateAppWidget(views);
    }

    private void updateAppWidget(RemoteViews views){
        ComponentName componentName = new ComponentName(this, MusicWidget.class);
        AppWidgetManager manager = AppWidgetManager.getInstance(this);
        manager.updateAppWidget(componentName, views);
    }

    //Creates // Updates
    private void showNotification(Track track, boolean isPlaying){

        if(track == null){
            stopForeground(true);
            return;
        }

        if(notification == null){
            notification = createNotification(track);
            startForeground(MUSIC_SERVICE_NOTIFICATION_ID, notification.getNotification());
        }else{
            notification.update(track, isPlaying);
        }

    }

    private TrackNotification createNotification(Track track){
        return new TrackNotification(this, MUSIC_SERVICE_NOTIFICATION_ID, track);
    }

    @Override
    public void onCompletionListener() {
        onStopTrackActionReceived();
    }

    private Runnable mUpdateTimeTask = new Runnable() {
        public void run() {

            if (musicPlayer != null) {

                long totalDuration = musicPlayer.getDuration();
                long currentDuration = musicPlayer.getCurrentPosition();

                // Displaying Total Duration time
                displayTrackElapsedTime(TrackUtils.getTimeElasped(totalDuration),
                        TrackUtils.getTimeElasped(currentDuration));

            }else{
                mHandler.removeCallbacks(mUpdateTimeTask);
            }


            // Running this thread after 100 milliseconds
            mHandler.postDelayed(this, 100);
        }
    };
}
