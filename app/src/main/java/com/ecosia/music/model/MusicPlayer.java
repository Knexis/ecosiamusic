package com.ecosia.music.model;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;

import com.ecosia.music.listener.MusicPlayerListener;
import com.ecosia.music.utils.TrackUtils;

import java.io.IOException;

public class MusicPlayer implements MediaPlayer.OnCompletionListener {
    private MediaPlayer player;
    private Context context;
    private MusicCursor musicCursor;

    private MusicPlayerListener listener;

    public MusicPlayer(Context context) {
        this.context = context;
        musicCursor = MusicCursor.newInstance(context);
    }

    private void initTrack(Track track) throws IOException {
        if(player == null){
            player = new MediaPlayer();
        }else{
            player.reset();
        }

        player.setOnCompletionListener(this);
        player.setAudioStreamType(AudioManager.STREAM_MUSIC);
        player.setDataSource(context, TrackUtils.getPathURI(track.getId()));
        player.prepare();
    }

    public void setListener(MusicPlayerListener listener) {
        this.listener = listener;
    }

    public boolean isPlaying(){
        return player != null && player.isPlaying();
    }

    public void play() throws IOException {
        if(player == null){
            initTrack(musicCursor.getNextTrack());
        }
        player.start();
    }

    public void pause(){
        if(isPlaying()){
            player.pause();
        }
    }

    public void stop(){
        if(player != null){
            player.stop();
            player.release();
            player = null;
        }
    }

    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {
        if(listener != null)
            listener.onCompletionListener();
    }

    public long getCurrentPosition() {
        return player.getCurrentPosition();
    }

    public long getDuration() {
        return player.getDuration();
    }
}
