package com.ecosia.music.model;

import android.content.Context;
import android.database.Cursor;
import android.provider.MediaStore;
import android.util.Log;

public class MusicCursor {
    private static final String TAG = MusicCursor.class.getSimpleName();

    private Context context;
    private Cursor cursor;

    private static MusicCursor musicCursor;

    private MusicCursor(Context context) {
        this.context = context;
    }

    //Return already created MusicCursor instance else create new instance
    public static MusicCursor newInstance(Context context){
        if(musicCursor == null){
            musicCursor = new MusicCursor(context);
            musicCursor.init();
        }
        return musicCursor;
    }

    private void init(){
        //Query: Randomly select music
        String sortOrder = "RANDOM()";
        //Query: Where audio is specifically marked as music by having is_music as true
        String selection = MediaStore.Audio.Media.IS_MUSIC + " != 0";

        //Query: List of metadata to retrieve from device's music library
        String[] projection = {
                MediaStore.Audio.Media._ID,
                MediaStore.Audio.Media.ARTIST,
                MediaStore.Audio.Media.TITLE,
                MediaStore.Audio.Media.DURATION
        };

        cursor = context.getContentResolver().query(
                MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                projection,
                selection,
                null,
                sortOrder
        );

        if(cursor != null)
            if(!cursor.moveToFirst()){
                Log.e(TAG, "no music found");
            }
    }//init


    //Returns a Track Object with the required metadata: id | Title | Artist name | Duration of track
    public Track getCurrentTrack(){
        long id       = cursor.getLong(cursor.getColumnIndex(android.provider.MediaStore.Audio.Media._ID));
        String title  = cursor.getString(cursor.getColumnIndex(android.provider.MediaStore.Audio.Media.TITLE));
        String artist = cursor.getString(cursor.getColumnIndex(android.provider.MediaStore.Audio.Media.ARTIST));
        long duration = cursor.getLong(cursor.getColumnIndex(android.provider.MediaStore.Audio.Media.DURATION));
        return new Track(id, title, artist, duration);
    }


    public Track getNextTrack(){

        //Point to the next track if possible otherwise move to the first track
        if(!cursor.moveToNext())
            cursor.moveToFirst();

        return getCurrentTrack();
    }
}//main
