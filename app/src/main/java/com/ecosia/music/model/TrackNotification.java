package com.ecosia.music.model;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import com.ecosia.music.widget.MusicWidget;
import com.ecosia.music.R;
import com.ecosia.music.service.MusicWidgetService;

public class TrackNotification {
    private String NOTIFICATION_CHANNEL_ID = "com.ecosia.music";

    private final int id;
    private Notification notification;
    private final NotificationCompat.Action actionPlayPause;

    private final NotificationManager manager;
    private final NotificationCompat.Builder builder;

    public TrackNotification(Context context, int id, Track track) {
        this.id = id;
        manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (manager != null) {
                manager.createNotificationChannel(createNotificationChannel());
            }
        }

        actionPlayPause = new NotificationCompat.Action(R.drawable.ic_pause_black_24dp, null,
                MusicWidget.createPendingIntent(context, MusicWidget.ACTION_PLAY_N_PAUSE));
        NotificationCompat.Action actionStop = new NotificationCompat.Action(R.drawable.ic_stop_black_24dp, null,
                MusicWidget.createPendingIntent(context, MusicWidget.ACTION_STOP));

        Intent intent = new Intent(context, MusicWidgetService.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);


        builder = new NotificationCompat.Builder(context, NOTIFICATION_CHANNEL_ID);
        //View controls on lock screen user settings hides sensitive content
        builder.setVisibility(NotificationCompat.VISIBILITY_PUBLIC);
        //Media controls
        builder.addAction(actionPlayPause).addAction(actionStop);

        builder.setPriority(NotificationCompat.PRIORITY_MAX);

        builder.setContentIntent(pendingIntent)
                .setSmallIcon(R.drawable.ic_stat_name)
                .setTicker(track.getTitle()).setOngoing(true)
                .setStyle(new androidx.media.app.NotificationCompat.MediaStyle());

        createBuilder(track);

        notification = builder.build();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private NotificationChannel createNotificationChannel(){
        String channelName = "MusicWidgetService";
        NotificationChannel channel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, channelName, NotificationManager.IMPORTANCE_HIGH);
        channel.setLightColor(Color.BLUE);
        channel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
        return channel;
    }

    private void createBuilder(Track track){
        builder.setWhen(System.currentTimeMillis())
                .setContentTitle(track.getTitle())
                .setContentText(track.getArtist());
    }

    public Notification getNotification(){
        return notification;
    }

    public void update(Track track, boolean isPlaying){
        createBuilder(track);
        actionPlayPause.icon = isPlaying? R.drawable.ic_play_arrow_black_24dp: R.drawable.ic_pause_black_24dp;
        notification = builder.build();
        manager.notify(id, notification);
    }

}//
