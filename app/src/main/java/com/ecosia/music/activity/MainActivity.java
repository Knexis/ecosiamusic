package com.ecosia.music.activity;

import android.Manifest;
import android.os.Bundle;

import com.ecosia.music.R;
import com.ecosia.music.activity.parent.ParentActivity;

public class MainActivity extends ParentActivity {

    //All permissions needed by current activity specified
    @Override
    protected String[] getRequiredPermissions() {
        return new String[]{
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.FOREGROUND_SERVICE
        };
    }

    //Called when all permissions are granted
    @Override
    protected void onAllRequiredPermissionsGranted() {
        super.onAllRequiredPermissionsGranted();
        tell("All permissions granted");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        checkIfHasAllRequiredPermissions();
    }
}
