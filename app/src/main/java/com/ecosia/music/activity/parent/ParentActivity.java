package com.ecosia.music.activity.parent;

import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.ecosia.music.utils.PermissionCheckUtils;

import java.util.ArrayList;

public abstract class ParentActivity extends AppCompatActivity {

    private static final int REQUEST_ALL_MISSING_PERMISSIONS = 1;


    /**
     * @return All permission which this activity needs
     */
    protected String[] getRequiredPermissions() {
        return new String[0];
    }

    /**
     * Called when all need permissions granted
     */
    protected void onAllRequiredPermissionsGranted() {
        // Do nothing here
    }

    public boolean checkIfHasAllRequiredPermissions(){
        if(hasAllRequiredPermissions()){
            return true;
        }
        else{
            requestAllRequiredPermissions();
            return false;
        }
    }

    protected boolean hasAllRequiredPermissions() {
        for (String permission : getRequiredPermissions()) {
            if (!PermissionCheckUtils.hasPermission(getApplicationContext(), permission)) {
                return false;
            }
        }
        return true;
    }

    @SuppressLint("NewApi")
    protected void requestAllRequiredPermissions() {
        ArrayList<String> notGrantedPermissions = new ArrayList<>();

        for (String permission : getRequiredPermissions()) {
            if (!PermissionCheckUtils.hasPermission(getApplicationContext(), permission)) {
                notGrantedPermissions.add(permission);
            }
        }

        if (notGrantedPermissions.size() > 0) {
            requestPermissions(notGrantedPermissions.toArray(new String[notGrantedPermissions.size()]),
                    REQUEST_ALL_MISSING_PERMISSIONS);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_ALL_MISSING_PERMISSIONS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    onAllRequiredPermissionsGranted();
                } else {
                    tell("Please allow all permissions");
                    finish();
                }
            }

        }
    }

    protected void tell(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
