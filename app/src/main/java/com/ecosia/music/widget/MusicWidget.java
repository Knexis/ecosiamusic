package com.ecosia.music.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;

import androidx.core.content.ContextCompat;

import com.ecosia.music.R;
import com.ecosia.music.service.MusicWidgetService;

/**
 * Implementation of App Widget functionality.
 */
public class MusicWidget extends AppWidgetProvider {

    private static final String TAG = MusicWidget.class.getSimpleName();

    private static int REQUEST_CODE = 991;
    public final static String ACTION_PLAY_N_PAUSE = "com.ecosia.music.play_n_pause";
    public final static String ACTION_STOP = "com.ecosia.music.stop";

    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                int appWidgetId) {
        // Construct the RemoteViews object
        RemoteViews views = getRemoteViews(context);

        // Instruct the widget manager to update the widget
        appWidgetManager.updateAppWidget(appWidgetId, views);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }

    public static PendingIntent createPendingIntent(Context context, String action) {
        Intent intent = new Intent(context, MusicWidget.class);
        intent.setAction(action);
        return PendingIntent.getBroadcast(context, REQUEST_CODE, intent, 0);
    }

    public static RemoteViews getRemoteViews(Context context){
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.music_widget);

        //Play/Pause button
        PendingIntent pendingIntentStart = createPendingIntent(context, MusicWidget.ACTION_PLAY_N_PAUSE);
        views.setOnClickPendingIntent(R.id.img_play_n_pause, pendingIntentStart);

        //Stop button
        PendingIntent pendingIntentStop = createPendingIntent(context, MusicWidget.ACTION_STOP);
        views.setOnClickPendingIntent(R.id.img_stop, pendingIntentStop);

        return views;
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        final String action = intent.getAction();
        Log.d(TAG, "Action: " + action);

        if(action == null)
            return;

        if ( (action.equals(ACTION_PLAY_N_PAUSE) || action.equals(ACTION_STOP)) ) {
            Intent serviceIntent = new Intent(context, MusicWidgetService.class);
            serviceIntent.setAction(action);
            ContextCompat.startForegroundService(context, serviceIntent);
        }
        else {
            super.onReceive(context, intent);
        }
    }
}

